import $ from 'jquery';
import { ApolloClient, HttpLink, InMemoryCache } from 'apollo-boost';
import Application from './application';

const PORT = 3000;
const client =new ApolloClient({
    link: new HttpLink({ uri: `http://localhost:${PORT}/api` }),
    cache: new InMemoryCache()
});

$(window).on('load', () => {
    const application = new Application(client);
});
