import CommerceSDK from 'commerce-sdk';

const resolvers = {
    Query: {
        productSearch: async (_, { query }, context) => {
            const clientConfig = await context.getClientConfig();
            const searchClient = new CommerceSDK.Search.ShopperSearch(clientConfig);
            let result;

            try {
                result = await searchClient.productSearch({ parameters: { q: query } });
            } catch (error) {
                console.log(error);
            }

            return result;
        }
    }
};

export default resolvers;
