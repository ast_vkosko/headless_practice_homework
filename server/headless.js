import CommerceSDK from 'commerce-sdk';

export default class Headless {
    get config() {
        return {
            headers: {},
            parameters: {
                siteId: process.env.COMMERCE_CLIENT_API_SITE_ID,
                clientId: process.env.COMMERCE_CLIENT_CLIENT_ID,
                organizationId: process.env.COMMERCE_CLIENT_ORGANIZATION_ID,
                shortCode: process.env.COMMERCE_CLIENT_SHORT_CODE
            }
        };
    }

    async getToken() {
        let clientConfig = this.config;
        let token;

        try {
            let response = await CommerceSDK.helpers.getShopperToken(clientConfig, { type: 'guest' });

            token = response.getBearerHeader();
        } catch (error) {
            return { error };
        }

        return { token };
    }
}
